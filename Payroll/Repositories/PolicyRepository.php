<?php

namespace Payroll\Repositories;

use Payroll\Models\Policy;
use Illuminate\Support\Facades\Cache;

class PolicyRepository
{
    public static function getCacheKey()
    {
        return database() . 'PAYROLL_POLICIES';
    }

    public static function reCache()
    {
        Cache::forget(self::getCacheKey());
        Cache::rememberForever(self::getCacheKey(), function () {
            return Policy::all();
        });
    }

    public static function checkCache()
    {
        if (! Cache::has(self::getCacheKey())) {
            self::reCache();
        }
    }

    public static function get($module, $policy)
    {
        self::checkCache();

        return Cache::get(self::getCacheKey())
            ->where('module_id', $module)
            ->where('policy', $policy)
            ->first()->value;
    }
}
