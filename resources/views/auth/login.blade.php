<!-- resources/views/auth/login.blade.php -->
@extends('layouts.auth')

@section('content')
<div class="flex items-center justify-center min-h-screen">
    <div class="card shadow-lg border-0 rounded-lg w-full max-w-md">
        <div class="card-header py-3 text-center bg-white border-b">
            <h3 class="text-2xl font-semibold mb-2">Sign-In</h3>
            <p class="text-sm text-gray-600">Welcome back! Please login to access your account.</p>
        </div>
        <div class="card-body p-6">
            <form method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}
            
                <!-- E-Mail Address Field -->
                <div class="mb-4">
                    <label for="email" class="block text-sm font-medium text-gray-700">E-Mail Address</label>
                    <div class="mt-1 flex rounded-md shadow-sm">
                        <span class="inline-flex items-center px-3 text-gray-500 bg-gray-100 border border-r-0 rounded-l-md">
                            <i class="fa-solid fa-envelope"></i>
                        </span>
                        <input 
                            type="email" 
                            id="email" 
                            class="form-control block w-full px-4 py-2 rounded-r-md border {{ $errors->has('email') ? 'border-red-500' : 'border-gray-300' }} focus:border-blue-500 focus:ring focus:ring-blue-200" 
                            name="email" 
                            value="{{ old('email') }}" 
                            required 
                            autofocus
                        >
                    </div>
                    @if ($errors->has('email'))
                        <div class="text-red-500 text-sm mt-1">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
                </div>
            
                <!-- Password Field -->
                <div class="mb-4">
                    <div class="flex justify-between items-center">
                        <label for="password" class="block text-sm font-medium text-gray-700">Password</label>
                        <a href="{{ url('/password/reset') }}" class="text-sm text-blue-500 hover:underline">
                            Forgot Your Password?
                        </a>
                    </div>
                    <div class="mt-1 flex rounded-md shadow-sm">
                        <span class="inline-flex items-center px-3 text-gray-500 bg-gray-100 border border-r-0 rounded-l-md">
                            <i class="fa-solid fa-lock"></i>
                        </span>
                        <input 
                            type="password" 
                            id="password" 
                            class="form-control block w-full px-4 py-2 rounded-r-md border {{ $errors->has('password') ? 'border-red-500' : 'border-gray-300' }} focus:border-blue-500 focus:ring focus:ring-blue-200" 
                            name="password" 
                            required
                        >
                    </div>
                    @if ($errors->has('password'))
                        <div class="text-red-500 text-sm mt-1">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif
                </div>                
            
                <!-- Remember Me Checkbox -->
                <div class="mb-4">
                    <div class="flex items-center">
                        <input 
                            class="form-check-input h-4 w-4 text-blue-500 border-gray-300 rounded focus:ring-blue-500" 
                            type="checkbox" 
                            name="remember" 
                            id="remember" 
                            {{ old('remember') ? 'checked' : '' }}
                        >
                        <label class="form-check-label text-sm ml-2" for="remember">
                            Remember Me
                        </label>
                    </div>
                </div>
            
                <!-- Submit Button -->
                <div class="mb-4">
                    <button type="submit" class="w-full bg-blue-500 text-white py-2 px-4 rounded-md hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-opacity-50 transition duration-150 ease-in-out">
                        <i class="fa-solid fa-right-to-bracket mr-2"></i>Sign In
                    </button>
                </div>
            </form>            
        </div>
        <div class="card-footer text-center py-4">
            <p class="mb-0">
                Don't have an account? 
                <a href="{{ url('/register') }}" class="text-blue-500 hover:underline">
                    Sign Up
                </a>
            </p>
        </div>
    </div>
</div>
@endsection
