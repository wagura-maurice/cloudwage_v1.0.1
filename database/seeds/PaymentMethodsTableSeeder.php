<?php

use Illuminate\Database\Seeder;

class PaymentMethodsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_methods')->delete();
        
        \DB::table('payment_methods')->insert(array (
            0 => 
            array (
                'id' => '1',
                'name' => 'Bank Transfer',
                'description' => 'bank transfer via pesalink',
                'has_udf' => '1',
                'coinage' => '0',
                'deleted_at' => NULL,
                'created_at' => '2024-12-07 12:18:27',
                'updated_at' => '2024-12-07 12:18:27',
            ),
            1 => 
            array (
                'id' => '2',
                'name' => 'Mobile Transfer',
                'description' => 'Mobile transfer via m-pesa',
                'has_udf' => '1',
                'coinage' => '0',
                'deleted_at' => '2024-12-07 12:22:39',
                'created_at' => '2024-12-07 12:22:39',
                'updated_at' => '2024-12-07 12:22:39',
            ),
        ));
        
        
    }
}