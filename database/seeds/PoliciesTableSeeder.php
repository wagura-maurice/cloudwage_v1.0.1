<?php

use Illuminate\Database\Seeder;

class PoliciesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('policies')->delete();
        
        \DB::table('policies')->insert(array (
            0 => 
            array (
                'id' => '1',
                'module_id' => '1',
                'policy' => 'ADVANCE FOR FUTURE',
                'value' => 'false',
                'postfix' => '',
                'exceptions' => '',
                'description' => 'This policy determines whether an advance can be taken on a month other than the current month.',
                'enabled' => '1',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            1 => 
            array (
                'id' => '2',
                'module_id' => '1',
                'policy' => 'MULTIPLE ADVANCES',
                'value' => 'false',
                'postfix' => '',
                'exceptions' => '',
                'description' => 'This policy determines whether an employee can take an advance when he has another unpaid advance.',
                'enabled' => '1',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            2 => 
            array (
                'id' => '3',
                'module_id' => '1',
                'policy' => 'ADVANCE MORE THAN BASIC',
                'value' => 'false',
                'postfix' => '',
                'exceptions' => '',
                'description' => 'This policy determines whether an employee can take an advance that is more than his basic pay.',
                'enabled' => '1',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            3 => 
            array (
                'id' => '4',
                'module_id' => '1',
                'policy' => 'GROSS PERCENTAGE AS ADVANCE',
                'value' => '50',
                'postfix' => '%',
                'exceptions' => '',
                'description' => 'This policy determines the maximum percentage of the gross pay that can be given as an advance.',
                'enabled' => '1',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            4 => 
            array (
                'id' => '5',
                'module_id' => '22',
                'policy' => 'PRESCRIBED LOAN RATE',
                'value' => '15',
                'postfix' => '%',
                'exceptions' => '',
                'description' => 'This policy refers to the prescribed loan rate set by KRA.',
                'enabled' => '1',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            5 => 
            array (
                'id' => '6',
                'module_id' => '29',
                'policy' => 'ENABLE DAYS WORKED MODULE',
                'value' => 'true',
                'postfix' => '',
                'exceptions' => '',
                'description' => 'This policy determines whether the time attendance Days module is enabled. Disabling it will configure the system to work as if all monthly paid employees were present for all days in a month',
                'enabled' => '1',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            6 => 
            array (
                'id' => '7',
                'module_id' => '29',
                'policy' => 'MINIMUM DAYS WORKED PER MONTH',
                'value' => '20',
                'postfix' => ' Days',
                'exceptions' => '',
                'description' => 'This policy defines the expected number of working days.',
                'enabled' => '1',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
        ));
        
        
    }
}