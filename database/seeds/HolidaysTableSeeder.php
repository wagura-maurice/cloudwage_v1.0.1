<?php

use Illuminate\Database\Seeder;

class HolidaysTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('holidays')->delete();
        
        \DB::table('holidays')->insert(array (
            0 => 
            array (
                'id' => '1',
                'name' => 'Jamuhuri Day',
                'slug' => NULL,
                'holiday_day' => '12',
                'holiday_week' => NULL,
                'holiday_month' => '12',
                'holiday_year' => NULL,
                'deleted_at' => NULL,
                'created_at' => '2024-12-07 11:40:46',
                'updated_at' => '2024-12-07 11:40:46',
            ),
            1 => 
            array (
                'id' => '2',
                'name' => 'Christmas Day',
                'slug' => NULL,
                'holiday_day' => '25',
                'holiday_week' => NULL,
                'holiday_month' => '12',
                'holiday_year' => NULL,
                'deleted_at' => NULL,
                'created_at' => '2024-12-07 11:41:15',
                'updated_at' => '2024-12-07 11:45:05',
            ),
            2 => 
            array (
                'id' => '3',
                'name' => 'Boxing Day',
                'slug' => NULL,
                'holiday_day' => '26',
                'holiday_week' => NULL,
                'holiday_month' => '12',
                'holiday_year' => NULL,
                'deleted_at' => NULL,
                'created_at' => '2024-12-07 11:41:24',
                'updated_at' => '2024-12-07 11:41:24',
            ),
            3 => 
            array (
                'id' => '4',
                'name' => 'New Year\'s Day',
                'slug' => NULL,
                'holiday_day' => '01',
                'holiday_week' => NULL,
                'holiday_month' => '01',
                'holiday_year' => NULL,
                'deleted_at' => NULL,
                'created_at' => '2024-12-07 11:41:36',
                'updated_at' => '2024-12-07 11:41:36',
            ),
        ));
        
        
    }
}