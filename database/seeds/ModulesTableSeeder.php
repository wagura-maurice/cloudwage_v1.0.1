<?php

use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('modules')->delete();
        
        \DB::table('modules')->insert(array (
            0 => 
            array (
                'id' => '1',
                'name' => 'Advances',
                'description' => 'This module deals with advance salary to the employees',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            1 => 
            array (
                'id' => '2',
                'name' => 'Advance Payments',
                'description' => 'This module deals with advance payments made by the employees',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            2 => 
            array (
                'id' => '3',
                'name' => 'Allowances',
                'description' => 'This module deals with allowances given to the employees',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            3 => 
            array (
                'id' => '4',
                'name' => 'Employee Assignment',
                'description' => 'This module deals with the assigning of an employee to a department',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            4 => 
            array (
                'id' => '5',
                'name' => 'Attendance',
                'description' => 'This module deals with the attendance of employees',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            5 => 
            array (
                'id' => '6',
                'name' => 'Company Profile',
                'description' => 'This module deals with the profile of the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            6 => 
            array (
                'id' => '7',
                'name' => 'Currencies',
                'description' => 'This module deals with the profile of the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            7 => 
            array (
                'id' => '8',
                'name' => 'Deductions',
                'description' => 'This module deals with the deductions to be used by organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            8 => 
            array (
                'id' => '9',
                'name' => 'Deduction Payments',
                'description' => 'This module deals with the deduction payments made by the employees',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            9 => 
            array (
                'id' => '10',
                'name' => 'Deduction Slabs',
                'description' => 'This module deals slabs that are attached to the deductions',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            10 => 
            array (
                'id' => '11',
                'name' => 'Departments',
                'description' => 'This module deals with the departments present in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            11 => 
            array (
                'id' => '12',
                'name' => 'Employees',
                'description' => 'This module deals with the employees in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            12 => 
            array (
                'id' => '13',
                'name' => 'Employee Allowances',
                'description' => 'This module deals with the allowances given to employees in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            13 => 
            array (
                'id' => '14',
                'name' => 'Employee Contracts',
                'description' => 'This module deals with the contracts of the employees in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            14 => 
            array (
                'id' => '15',
                'name' => 'Employee Deductions',
                'description' => 'This module deals with the deductions assigned to employees in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            15 => 
            array (
                'id' => '16',
                'name' => 'Employee Payment Methods',
                'description' => 'This module deals with the payment methods used by employees in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            16 => 
            array (
                'id' => '17',
                'name' => 'Employee Reliefs',
                'description' => 'This module deals with the reliefs given to the employees in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            17 => 
            array (
                'id' => '18',
                'name' => 'Employee Types',
                'description' => 'This module deals with the departments present in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            18 => 
            array (
                'id' => '19',
                'name' => 'Employee Work Plan Assignment',
                'description' => 'This module deals with assignment of work plans to employees in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            19 => 
            array (
                'id' => '20',
                'name' => 'Holidays',
                'description' => 'This module deals with the holidays considered in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            20 => 
            array (
                'id' => '21',
                'name' => 'Loan Payments',
                'description' => 'This module deals with the payments against loans in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            21 => 
            array (
                'id' => '22',
                'name' => 'Loans',
                'description' => 'This module deals with loans given to employees in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            22 => 
            array (
                'id' => '23',
                'name' => 'Modules',
                'description' => 'This module deals with the modules present in the system',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            23 => 
            array (
                'id' => '24',
                'name' => 'Notifications',
                'description' => 'This module deals with the notifications sent to the users of the system',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            24 => 
            array (
                'id' => '25',
                'name' => 'Notification Types',
                'description' => 'This module deals with the types of notifications present in the system',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            25 => 
            array (
                'id' => '26',
                'name' => 'Pay Grades',
                'description' => 'This module deals with the pay grades used in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            26 => 
            array (
                'id' => '27',
                'name' => 'Payment Methods',
                'description' => 'This module deals with the payment methods used in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            27 => 
            array (
                'id' => '28',
                'name' => 'Payment Structures',
                'description' => 'This module deals with the structures used to pay employees in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            28 => 
            array (
                'id' => '29',
                'name' => 'Payroll',
                'description' => 'This module deals with the generation of payslips',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            29 => 
            array (
                'id' => '30',
                'name' => 'Policies',
                'description' => 'This module deals with the policies governing the system',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            30 => 
            array (
                'id' => '31',
                'name' => 'Relief',
                'description' => 'This module deals with the reliefs used in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            31 => 
            array (
                'id' => '32',
                'name' => 'Report Templates',
                'description' => 'This module deals with the templates used to generate reports',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            32 => 
            array (
                'id' => '33',
                'name' => 'Settings',
                'description' => 'This module deals with the settings of the system',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            33 => 
            array (
                'id' => '34',
                'name' => 'Shifts',
                'description' => 'This module deals with the shifts present in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            34 => 
            array (
                'id' => '35',
                'name' => 'Template',
                'description' => 'This module deals with the templates used in the system',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            35 => 
            array (
                'id' => '36',
                'name' => 'UDFs',
                'description' => 'This module deals with the user defined fields present in the system',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            36 => 
            array (
                'id' => '37',
                'name' => 'Work Plans',
                'description' => 'This module deals with the work plans present in the organization',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
        ));
        
        
    }
}