<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CompanyProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company_profiles')->insert([
            'logo' => 'images/smalllogo.jpg',
            'name' => 'Support Cental Solutions',
            'city' => 'Nairobi',
            'country' => 'Kenya',
            'phone' => '00800-XXXXXXX',
            'mobile' => '07XXXXXXXX / 07XXXXXXX',
            'email' => 'info@supportcental.co.ke',
            'website' => 'http://cloudwage.supportcental.co.ke',
            'kra_pin' => ' ',
            'currency_id' => '75',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
