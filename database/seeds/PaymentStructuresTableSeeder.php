<?php

use Illuminate\Database\Seeder;

class PaymentStructuresTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_structures')->delete();
        
        \DB::table('payment_structures')->insert(array (
            0 => 
            array (
                'id' => '1',
                'name' => 'Monthly Salary Payment',
                'description' => 'Monthly Salary Payment',
                'unit' => 'Month',
                'created_at' => '2024-12-07 12:23:48',
                'updated_at' => '2024-12-07 12:23:48',
            ),
        ));
        
        
    }
}