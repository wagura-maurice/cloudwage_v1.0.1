<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('departments')->delete();
        
        \DB::table('departments')->insert(array (
            0 => 
            array (
                'id' => '1',
                'branch_id' => '1',
                'parent_id' => NULL,
                'name' => 'Production & Operations Department',
                'created_at' => '2024-12-07 13:31:40',
                'updated_at' => '2024-12-07 13:31:40',
            ),
            1 => 
            array (
                'id' => '2',
                'branch_id' => '1',
                'parent_id' => NULL,
                'name' => 'Engineering & Maintenance Department',
                'created_at' => '2024-12-07 13:31:51',
                'updated_at' => '2024-12-07 13:31:51',
            ),
            2 => 
            array (
                'id' => '3',
                'branch_id' => '1',
                'parent_id' => NULL,
                'name' => 'Quality Control & Assurance Department',
                'created_at' => '2024-12-07 13:32:04',
                'updated_at' => '2024-12-07 13:32:04',
            ),
            3 => 
            array (
                'id' => '4',
                'branch_id' => '1',
                'parent_id' => NULL,
                'name' => 'Logistics & Supply Chain Management',
                'created_at' => '2024-12-07 13:32:16',
                'updated_at' => '2024-12-07 13:32:16',
            ),
            4 => 
            array (
                'id' => '5',
                'branch_id' => '1',
                'parent_id' => NULL,
                'name' => 'Administrative Department',
                'created_at' => '2024-12-07 13:32:27',
                'updated_at' => '2024-12-07 13:32:27',
            ),
            5 => 
            array (
                'id' => '6',
                'branch_id' => '1',
                'parent_id' => NULL,
                'name' => 'Finance & Accounting Department',
                'created_at' => '2024-12-07 13:32:38',
                'updated_at' => '2024-12-07 13:32:38',
            ),
            6 => 
            array (
                'id' => '7',
                'branch_id' => '1',
                'parent_id' => NULL,
                'name' => 'Sales & Marketing Department',
                'created_at' => '2024-12-07 13:32:48',
                'updated_at' => '2024-12-07 13:32:48',
            ),
            7 => 
            array (
                'id' => '8',
                'branch_id' => '1',
                'parent_id' => NULL,
            'name' => 'Research & Development (R&D) Department',
                'created_at' => '2024-12-07 13:33:05',
                'updated_at' => '2024-12-07 13:33:05',
            ),
            8 => 
            array (
                'id' => '9',
                'branch_id' => '1',
                'parent_id' => NULL,
            'name' => 'Health, Safety & Environment (HSE) Department',
                'created_at' => '2024-12-07 13:33:17',
                'updated_at' => '2024-12-07 13:33:17',
            ),
            9 => 
            array (
                'id' => '10',
                'branch_id' => '1',
                'parent_id' => NULL,
                'name' => 'IT & Data Management Department',
                'created_at' => '2024-12-07 13:33:30',
                'updated_at' => '2024-12-07 13:33:30',
            ),
            10 => 
            array (
                'id' => '11',
                'branch_id' => '1',
                'parent_id' => NULL,
                'name' => 'Support Services Department',
                'created_at' => '2024-12-07 13:33:40',
                'updated_at' => '2024-12-07 13:33:40',
            ),
            11 => 
            array (
                'id' => '12',
                'branch_id' => '1',
                'parent_id' => NULL,
                'name' => 'Compliance & Legal Affairs Department',
                'created_at' => '2024-12-07 13:33:50',
                'updated_at' => '2024-12-07 13:33:50',
            ),
            12 => 
            array (
                'id' => '13',
                'branch_id' => '1',
                'parent_id' => NULL,
                'name' => 'Plant Management Office',
                'created_at' => '2024-12-07 13:34:01',
                'updated_at' => '2024-12-07 13:34:01',
            ),
        ));
        
        
    }
}