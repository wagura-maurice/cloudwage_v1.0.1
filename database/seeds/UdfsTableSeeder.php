<?php

use Illuminate\Database\Seeder;

class UdfsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('udfs')->delete();
        
        \DB::table('udfs')->insert(array (
            0 => 
            array (
                'id' => '1',
                'udfable_id' => '2',
                'udfable_type' => 'Payroll\\Models\\PaymentMethod',
                'field_title' => 'Account Number',
                'field_name' => 'account-number',
                'created_at' => '2024-12-07 12:18:27',
                'updated_at' => '2024-12-07 12:18:27',
            ),
            1 => 
            array (
                'id' => '2',
                'udfable_id' => '2',
                'udfable_type' => 'Payroll\\Models\\PaymentMethod',
                'field_title' => 'Bank Name',
                'field_name' => 'bank-name',
                'created_at' => '2024-12-07 12:18:27',
                'updated_at' => '2024-12-07 12:18:27',
            ),
            2 => 
            array (
                'id' => '3',
                'udfable_id' => '2',
                'udfable_type' => 'Payroll\\Models\\PaymentMethod',
                'field_title' => 'Branch Name',
                'field_name' => 'branch-name',
                'created_at' => '2024-12-07 12:18:27',
                'updated_at' => '2024-12-07 12:18:27',
            ),
            3 => 
            array (
                'id' => '4',
                'udfable_id' => '3',
                'udfable_type' => 'Payroll\\Models\\PaymentMethod',
                'field_title' => 'Phone Number',
                'field_name' => 'phone-number',
                'created_at' => '2024-12-07 12:22:39',
                'updated_at' => '2024-12-07 12:22:39',
            ),
        ));
        
        
    }
}