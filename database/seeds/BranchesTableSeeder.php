<?php

use Illuminate\Database\Seeder;

class BranchesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('branches')->delete();
        
        \DB::table('branches')->insert(array (
            0 => 
            array (
                'id' => '1',
                'branch_name' => 'Main',
                'location' => 'Nairobi',
                'created_at' => '2024-12-07 13:27:58',
                'updated_at' => '2024-12-07 13:27:58',
            ),
        ));
        
        
    }
}