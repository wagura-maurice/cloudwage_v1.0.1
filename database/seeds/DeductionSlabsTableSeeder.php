<?php

use Illuminate\Database\Seeder;

class DeductionSlabsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('deduction_slabs')->delete();
        
        \DB::table('deduction_slabs')->insert(array (
            0 => 
            array (
                'id' => '1',
                'deduction_id' => '1',
                'slab_number' => '1',
                'min_amount' => '0.0',
                'max_amount' => '11180.0',
                'rate' => '10%',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            1 => 
            array (
                'id' => '2',
                'deduction_id' => '1',
                'slab_number' => '2',
                'min_amount' => '11181.0',
                'max_amount' => '21714.0',
                'rate' => '15%',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            2 => 
            array (
                'id' => '3',
                'deduction_id' => '1',
                'slab_number' => '3',
                'min_amount' => '21715.0',
                'max_amount' => '32248.0',
                'rate' => '20%',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            3 => 
            array (
                'id' => '4',
                'deduction_id' => '1',
                'slab_number' => '4',
                'min_amount' => '32249.0',
                'max_amount' => '42781.0',
                'rate' => '25%',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            4 => 
            array (
                'id' => '5',
                'deduction_id' => '1',
                'slab_number' => '5',
                'min_amount' => '42782.0',
                'max_amount' => '0.0',
                'rate' => '30%',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            5 => 
            array (
                'id' => '6',
                'deduction_id' => '2',
                'slab_number' => '1',
                'min_amount' => '0.0',
                'max_amount' => '5999.0',
                'rate' => '150',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            6 => 
            array (
                'id' => '7',
                'deduction_id' => '2',
                'slab_number' => '2',
                'min_amount' => '6000.0',
                'max_amount' => '7999.0',
                'rate' => '300',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            7 => 
            array (
                'id' => '8',
                'deduction_id' => '2',
                'slab_number' => '3',
                'min_amount' => '8000.0',
                'max_amount' => '11999.0',
                'rate' => '400',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            8 => 
            array (
                'id' => '9',
                'deduction_id' => '2',
                'slab_number' => '4',
                'min_amount' => '12000.0',
                'max_amount' => '14999.0',
                'rate' => '500',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            9 => 
            array (
                'id' => '10',
                'deduction_id' => '2',
                'slab_number' => '5',
                'min_amount' => '15000.0',
                'max_amount' => '19999.0',
                'rate' => '600',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            10 => 
            array (
                'id' => '11',
                'deduction_id' => '2',
                'slab_number' => '6',
                'min_amount' => '20000.0',
                'max_amount' => '24999.0',
                'rate' => '750',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            11 => 
            array (
                'id' => '12',
                'deduction_id' => '2',
                'slab_number' => '7',
                'min_amount' => '25000.0',
                'max_amount' => '29999.0',
                'rate' => '850',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            12 => 
            array (
                'id' => '13',
                'deduction_id' => '2',
                'slab_number' => '8',
                'min_amount' => '30000.0',
                'max_amount' => '34999.0',
                'rate' => '900',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            13 => 
            array (
                'id' => '14',
                'deduction_id' => '2',
                'slab_number' => '9',
                'min_amount' => '35000.0',
                'max_amount' => '39999.0',
                'rate' => '950',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            14 => 
            array (
                'id' => '15',
                'deduction_id' => '2',
                'slab_number' => '10',
                'min_amount' => '40000.0',
                'max_amount' => '44999.0',
                'rate' => '1000',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            15 => 
            array (
                'id' => '16',
                'deduction_id' => '2',
                'slab_number' => '11',
                'min_amount' => '45000.0',
                'max_amount' => '49999.0',
                'rate' => '1100',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            16 => 
            array (
                'id' => '17',
                'deduction_id' => '2',
                'slab_number' => '12',
                'min_amount' => '50000.0',
                'max_amount' => '59999.0',
                'rate' => '1200',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            17 => 
            array (
                'id' => '18',
                'deduction_id' => '2',
                'slab_number' => '13',
                'min_amount' => '60000.0',
                'max_amount' => '69999.0',
                'rate' => '1300',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            18 => 
            array (
                'id' => '19',
                'deduction_id' => '2',
                'slab_number' => '14',
                'min_amount' => '70000.0',
                'max_amount' => '79999.0',
                'rate' => '1400',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            19 => 
            array (
                'id' => '20',
                'deduction_id' => '2',
                'slab_number' => '15',
                'min_amount' => '80000.0',
                'max_amount' => '89999.0',
                'rate' => '1500',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            20 => 
            array (
                'id' => '21',
                'deduction_id' => '2',
                'slab_number' => '16',
                'min_amount' => '90000.0',
                'max_amount' => '99999.0',
                'rate' => '1600',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            21 => 
            array (
                'id' => '22',
                'deduction_id' => '2',
                'slab_number' => '17',
                'min_amount' => '100000.0',
                'max_amount' => '0.0',
                'rate' => '1700',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            22 => 
            array (
                'id' => '23',
                'deduction_id' => '3',
                'slab_number' => '1',
                'min_amount' => '0.0',
                'max_amount' => '6000.0',
                'rate' => '6',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            23 => 
            array (
                'id' => '24',
                'deduction_id' => '3',
                'slab_number' => '2',
                'min_amount' => '6001.0',
                'max_amount' => '18000.0',
                'rate' => '6',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
        ));
        
        
    }
}