<?php

use Illuminate\Database\Seeder;

class ReliefsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('reliefs')->delete();
        
        \DB::table('reliefs')->insert(array (
            0 => 
            array (
                'id' => '1',
                'name' => 'Personal Relief',
                'reliefable_id' => '1',
                'reliefable_type' => 'Deduction',
                'amount' => '1280.0',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
        ));
        
        
    }
}