<?php

use Illuminate\Database\Seeder;

class ShiftsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('shifts')->delete();
        
        \DB::table('shifts')->insert(array (
            0 => 
            array (
                'id' => '1',
                'name' => '8 Hrs Day Shift',
                'is_overnight' => '0',
                'shift_start' => '8:00',
                'shift_end' => '17:00',
                'shift_hours' => '540.0',
                'time_allowance' => '20',
                'breaks' => '60',
                'created_at' => '2024-12-07 11:16:33',
                'updated_at' => '2024-12-07 11:16:33',
            ),
            1 => 
            array (
                'id' => '2',
                'name' => '12 Hrs Day Shift',
                'is_overnight' => '0',
                'shift_start' => '6:00',
                'shift_end' => '18:00',
                'shift_hours' => '720.0',
                'time_allowance' => '20',
                'breaks' => '60',
                'created_at' => '2024-12-07 11:17:01',
                'updated_at' => '2024-12-07 11:17:01',
            ),
            2 => 
            array (
                'id' => '3',
                'name' => '12 Hrs Night Shift',
                'is_overnight' => '1',
                'shift_start' => '18:00',
                'shift_end' => '6:00',
                'shift_hours' => '720.0',
                'time_allowance' => '20',
                'breaks' => '0',
                'created_at' => '2024-12-07 11:17:27',
                'updated_at' => '2024-12-07 11:17:27',
            ),
            3 => 
            array (
                'id' => '4',
                'name' => 'Weekend Shift',
                'is_overnight' => '0',
                'shift_start' => '8:00',
                'shift_end' => '13:00',
                'shift_hours' => '300.0',
                'time_allowance' => '20',
                'breaks' => '0',
                'created_at' => '2024-12-07 11:18:01',
                'updated_at' => '2024-12-07 11:18:01',
            ),
        ));
        
        
    }
}