<?php

use Illuminate\Database\Seeder;

class EmployeeTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('employee_types')->delete();
        
        \DB::table('employee_types')->insert(array (
            0 => 
            array (
                'id' => '1',
                'payment_structure_id' => '1',
                'name' => 'Factory-Level Employees',
                'description' => 'Machine Operators: Operate machinery for sugarcane crushing and processing.
Manual Laborers: Perform physical tasks like loading/unloading and cleaning.
Maintenance Staff: Handle basic factory upkeep and machinery maintenance.',
                'created_at' => '2024-12-07 13:00:25',
                'updated_at' => '2024-12-07 13:04:03',
            ),
            1 => 
            array (
                'id' => '2',
                'payment_structure_id' => '1',
                'name' => 'Technical and Engineering Staff',
                'description' => 'Mechanical Engineers: Oversee and repair mechanical systems.
Electrical Engineers: Maintain and troubleshoot electrical equipment.
Process Engineers: Optimize production processes.
Technicians: Assist engineers with daily operational tasks.',
                'created_at' => '2024-12-07 13:04:25',
                'updated_at' => '2024-12-07 13:04:25',
            ),
            2 => 
            array (
                'id' => '3',
                'payment_structure_id' => '1',
            'name' => 'Quality Control (QC) Staff',
                'description' => 'Chemists: Monitor sugar production quality.
Laboratory Technicians: Conduct tests on raw and processed materials.',
                'created_at' => '2024-12-07 13:04:46',
                'updated_at' => '2024-12-07 13:04:46',
            ),
            3 => 
            array (
                'id' => '4',
                'payment_structure_id' => '1',
                'name' => 'Logistics and Warehouse Staff',
                'description' => 'Warehouse Supervisors: Manage inventory and storage.
Logistics Coordinators: Plan and execute product shipments.',
                'created_at' => '2024-12-07 13:05:03',
                'updated_at' => '2024-12-07 13:05:03',
            ),
            4 => 
            array (
                'id' => '5',
                'payment_structure_id' => '1',
                'name' => 'Administrative Staff',
                'description' => 'Office Assistants: Handle day-to-day office tasks.
Accountants: Manage financial records and transactions.
HR Personnel: Recruit, onboard, and manage employee welfare.',
                'created_at' => '2024-12-07 13:05:24',
                'updated_at' => '2024-12-07 13:05:24',
            ),
            5 => 
            array (
                'id' => '6',
                'payment_structure_id' => '1',
                'name' => 'Supervisors and Management',
                'description' => 'Shift Supervisors: Oversee specific shifts to ensure smooth operations.
Department Heads: Lead specific sections like engineering, quality, or administration.
Operations Managers: Coordinate across departments to meet production goals.',
                'created_at' => '2024-12-07 13:05:46',
                'updated_at' => '2024-12-07 13:05:46',
            ),
            6 => 
            array (
                'id' => '7',
                'payment_structure_id' => '1',
                'name' => 'Support Staff',
                'description' => 'Cooks/House Servants: Provide catering and cleaning services for staff residences.
Gardeners: Maintain the plant’s surroundings.
Watchmen (Day/Night): Provide security for the facility.
Drivers: Transport goods, staff, and supplies.',
                'created_at' => '2024-12-07 13:06:05',
                'updated_at' => '2024-12-07 13:06:05',
            ),
            7 => 
            array (
                'id' => '8',
                'payment_structure_id' => '1',
                'name' => 'Senior Management',
                'description' => 'Plant Manager: Overall in charge of the facility\'s operations.
General Manager: Focus on long-term goals and external stakeholder management.',
                'created_at' => '2024-12-07 13:06:23',
                'updated_at' => '2024-12-07 13:06:23',
            ),
            8 => 
            array (
                'id' => '9',
                'payment_structure_id' => '1',
                'name' => 'Specialized Roles',
                'description' => 'Tax and Compliance Officers: Handle taxation and regulatory requirements.
Safety Officers: Ensure adherence to health and safety standards.',
                'created_at' => '2024-12-07 13:06:41',
                'updated_at' => '2024-12-07 13:06:41',
            ),
        ));
        
        
    }
}