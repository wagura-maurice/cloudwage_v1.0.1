<?php

use Illuminate\Database\Seeder;

class PayGradesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pay_grades')->delete();
        
        \DB::table('pay_grades')->insert(array (
            0 => 
            array (
                'id' => '1',
                'name' => 'G1',
                'payment_structure_id' => '1',
                'currency_id' => '75',
                'basic_salary' => '25000.0',
                'annual_increment' => '10',
                'default_allowances' => '7,8,9',
                'default_deductions' => '1,2,3',
                'created_at' => '2024-12-07 12:38:57',
                'updated_at' => '2024-12-07 12:40:20',
            ),
            1 => 
            array (
                'id' => '2',
                'name' => 'G2',
                'payment_structure_id' => '1',
                'currency_id' => '75',
                'basic_salary' => '50000.0',
                'annual_increment' => '10',
                'default_allowances' => '7,8,9,10,11,13',
                'default_deductions' => '1,2,3',
                'created_at' => '2024-12-07 12:40:06',
                'updated_at' => '2024-12-07 12:40:06',
            ),
            2 => 
            array (
                'id' => '3',
                'name' => 'G3',
                'payment_structure_id' => '1',
                'currency_id' => '75',
                'basic_salary' => '60000.0',
                'annual_increment' => '10',
                'default_allowances' => '6,7,8,9,10,11,14',
                'default_deductions' => '1,2,3',
                'created_at' => '2024-12-07 12:43:29',
                'updated_at' => '2024-12-07 12:43:29',
            ),
            3 => 
            array (
                'id' => '4',
                'name' => 'G4',
                'payment_structure_id' => '1',
                'currency_id' => '75',
                'basic_salary' => '30000.0',
                'annual_increment' => '8',
                'default_allowances' => '7,8,9',
                'default_deductions' => '1,2,3',
                'created_at' => '2024-12-07 12:44:17',
                'updated_at' => '2024-12-07 12:44:17',
            ),
            4 => 
            array (
                'id' => '5',
                'name' => 'G5',
                'payment_structure_id' => '1',
                'currency_id' => '75',
                'basic_salary' => '40000.0',
                'annual_increment' => '8',
                'default_allowances' => '4,7,8,9,10',
                'default_deductions' => '1,2,3',
                'created_at' => '2024-12-07 12:45:16',
                'updated_at' => '2024-12-07 12:45:16',
            ),
            5 => 
            array (
                'id' => '6',
                'name' => 'G6',
                'payment_structure_id' => '1',
                'currency_id' => '75',
                'basic_salary' => '70000.0',
                'annual_increment' => '12',
                'default_allowances' => '7,8,9,10,11,15',
                'default_deductions' => '1,2,3',
                'created_at' => '2024-12-07 12:46:44',
                'updated_at' => '2024-12-07 12:46:44',
            ),
            6 => 
            array (
                'id' => '7',
                'name' => 'G7',
                'payment_structure_id' => '1',
                'currency_id' => '75',
                'basic_salary' => '120000.0',
                'annual_increment' => '15',
                'default_allowances' => '7,8,9,10,11,17',
                'default_deductions' => '1,2,3',
                'created_at' => '2024-12-07 12:47:30',
                'updated_at' => '2024-12-07 12:47:30',
            ),
        ));
        
        
    }
}