<?php

use Illuminate\Database\Seeder;

class WorkPlansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('work_plans')->delete();
        
        \DB::table('work_plans')->insert(array (
            0 => 
            array (
                'id' => '1',
                'name' => '8 Hrs Weekday Workplan',
                'shift_id' => '1',
                'days_of_week' => 'Monday,Tuesday,Wednesday,Thursday,Friday',
                'created_at' => '2024-12-07 11:18:54',
                'updated_at' => '2024-12-07 11:18:54',
            ),
            1 => 
            array (
                'id' => '2',
                'name' => '12 Hrs Weekday Workplan',
                'shift_id' => '2',
                'days_of_week' => 'Monday,Tuesday,Wednesday,Thursday,Friday',
                'created_at' => '2024-12-07 11:19:07',
                'updated_at' => '2024-12-07 11:19:07',
            ),
            2 => 
            array (
                'id' => '3',
                'name' => '12 Hrs Weeknight Workplan',
                'shift_id' => '3',
                'days_of_week' => 'Monday,Tuesday,Wednesday,Thursday,Friday',
                'created_at' => '2024-12-07 11:19:28',
                'updated_at' => '2024-12-07 11:19:28',
            ),
            3 => 
            array (
                'id' => '4',
                'name' => '5 Hrs Weekend Workplan',
                'shift_id' => '4',
                'days_of_week' => 'Saturday,Sunday',
                'created_at' => '2024-12-07 11:19:56',
                'updated_at' => '2024-12-07 11:19:56',
            ),
        ));
        
        
    }
}