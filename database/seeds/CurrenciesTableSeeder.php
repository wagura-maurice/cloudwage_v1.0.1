<?php

use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('currencies')->delete();
        
        \DB::table('currencies')->insert(array (
            0 => 
            array (
                'id' => '1',
                'code' => 'AED',
                'name' => 'United Arab Emirates Dirham',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            1 => 
            array (
                'id' => '2',
                'code' => 'AFN',
                'name' => 'Afghan Afghani',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            2 => 
            array (
                'id' => '3',
                'code' => 'ALL',
                'name' => 'Albanian Lek',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            3 => 
            array (
                'id' => '4',
                'code' => 'AMD',
                'name' => 'Armenian Dram',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            4 => 
            array (
                'id' => '5',
                'code' => 'ANG',
                'name' => 'Netherlands Antillean Guilder',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            5 => 
            array (
                'id' => '6',
                'code' => 'AOA',
                'name' => 'Angolan Kwanza',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            6 => 
            array (
                'id' => '7',
                'code' => 'ARS',
                'name' => 'Argentine Peso',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            7 => 
            array (
                'id' => '8',
                'code' => 'AUD',
                'name' => 'Australian Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            8 => 
            array (
                'id' => '9',
                'code' => 'AWG',
                'name' => 'Aruban Florin',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            9 => 
            array (
                'id' => '10',
                'code' => 'AZN',
                'name' => 'Azerbaijani Manat',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            10 => 
            array (
                'id' => '11',
                'code' => 'BAM',
                'name' => 'Bosnia-Herzegovina Convertible Mark',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            11 => 
            array (
                'id' => '12',
                'code' => 'BBD',
                'name' => 'Barbadian Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            12 => 
            array (
                'id' => '13',
                'code' => 'BDT',
                'name' => 'Bangladeshi Taka',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            13 => 
            array (
                'id' => '14',
                'code' => 'BGN',
                'name' => 'Bulgarian Lev',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            14 => 
            array (
                'id' => '15',
                'code' => 'BHD',
                'name' => 'Bahraini Dinar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            15 => 
            array (
                'id' => '16',
                'code' => 'BIF',
                'name' => 'Burundian Franc',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            16 => 
            array (
                'id' => '17',
                'code' => 'BMD',
                'name' => 'Bermudan Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            17 => 
            array (
                'id' => '18',
                'code' => 'BND',
                'name' => 'Brunei Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            18 => 
            array (
                'id' => '19',
                'code' => 'BOB',
                'name' => 'Bolivian Boliviano',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            19 => 
            array (
                'id' => '20',
                'code' => 'BRL',
                'name' => 'Brazilian Real',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            20 => 
            array (
                'id' => '21',
                'code' => 'BSD',
                'name' => 'Bahamian Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            21 => 
            array (
                'id' => '22',
                'code' => 'BTC',
                'name' => 'Bitcoin',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            22 => 
            array (
                'id' => '23',
                'code' => 'BTN',
                'name' => 'Bhutanese Ngultrum',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            23 => 
            array (
                'id' => '24',
                'code' => 'BWP',
                'name' => 'Botswanan Pula',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            24 => 
            array (
                'id' => '25',
                'code' => 'BYR',
                'name' => 'Belarusian Ruble',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            25 => 
            array (
                'id' => '26',
                'code' => 'BZD',
                'name' => 'Belize Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            26 => 
            array (
                'id' => '27',
                'code' => 'CAD',
                'name' => 'Canadian Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            27 => 
            array (
                'id' => '28',
                'code' => 'CDF',
                'name' => 'Congolese Franc',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            28 => 
            array (
                'id' => '29',
                'code' => 'CHF',
                'name' => 'Swiss Franc',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            29 => 
            array (
                'id' => '30',
                'code' => 'CLF',
            'name' => 'Chilean Unit of Account (UF)',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            30 => 
            array (
                'id' => '31',
                'code' => 'CLP',
                'name' => 'Chilean Peso',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            31 => 
            array (
                'id' => '32',
                'code' => 'CNY',
                'name' => 'Chinese Yuan',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            32 => 
            array (
                'id' => '33',
                'code' => 'COP',
                'name' => 'Colombian Peso',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            33 => 
            array (
                'id' => '34',
                'code' => 'CRC',
                'name' => 'Costa Rican Colón',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            34 => 
            array (
                'id' => '35',
                'code' => 'CUC',
                'name' => 'Cuban Convertible Peso',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            35 => 
            array (
                'id' => '36',
                'code' => 'CUP',
                'name' => 'Cuban Peso',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            36 => 
            array (
                'id' => '37',
                'code' => 'CVE',
                'name' => 'Cape Verdean Escudo',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            37 => 
            array (
                'id' => '38',
                'code' => 'CZK',
                'name' => 'Czech Republic Koruna',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            38 => 
            array (
                'id' => '39',
                'code' => 'DJF',
                'name' => 'Djiboutian Franc',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            39 => 
            array (
                'id' => '40',
                'code' => 'DKK',
                'name' => 'Danish Krone',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            40 => 
            array (
                'id' => '41',
                'code' => 'DOP',
                'name' => 'Dominican Peso',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            41 => 
            array (
                'id' => '42',
                'code' => 'DZD',
                'name' => 'Algerian Dinar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            42 => 
            array (
                'id' => '43',
                'code' => 'EEK',
                'name' => 'Estonian Kroon',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            43 => 
            array (
                'id' => '44',
                'code' => 'EGP',
                'name' => 'Egyptian Pound',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            44 => 
            array (
                'id' => '45',
                'code' => 'ERN',
                'name' => 'Eritrean Nakfa',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            45 => 
            array (
                'id' => '46',
                'code' => 'ETB',
                'name' => 'Ethiopian Birr',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            46 => 
            array (
                'id' => '47',
                'code' => 'EUR',
                'name' => 'Euro',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            47 => 
            array (
                'id' => '48',
                'code' => 'FJD',
                'name' => 'Fijian Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            48 => 
            array (
                'id' => '49',
                'code' => 'FKP',
                'name' => 'Falkland Islands Pound',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            49 => 
            array (
                'id' => '50',
                'code' => 'GBP',
                'name' => 'British Pound Sterling',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            50 => 
            array (
                'id' => '51',
                'code' => 'GEL',
                'name' => 'Georgian Lari',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            51 => 
            array (
                'id' => '52',
                'code' => 'GGP',
                'name' => 'Guernsey Pound',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            52 => 
            array (
                'id' => '53',
                'code' => 'GHS',
                'name' => 'Ghanaian Cedi',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            53 => 
            array (
                'id' => '54',
                'code' => 'GIP',
                'name' => 'Gibraltar Pound',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            54 => 
            array (
                'id' => '55',
                'code' => 'GMD',
                'name' => 'Gambian Dalasi',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            55 => 
            array (
                'id' => '56',
                'code' => 'GNF',
                'name' => 'Guinean Franc',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            56 => 
            array (
                'id' => '57',
                'code' => 'GTQ',
                'name' => 'Guatemalan Quetzal',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            57 => 
            array (
                'id' => '58',
                'code' => 'GYD',
                'name' => 'Guyanaese Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            58 => 
            array (
                'id' => '59',
                'code' => 'HKD',
                'name' => 'Hong Kong Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            59 => 
            array (
                'id' => '60',
                'code' => 'HNL',
                'name' => 'Honduran Lempira',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            60 => 
            array (
                'id' => '61',
                'code' => 'HRK',
                'name' => 'Croatian Kuna',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            61 => 
            array (
                'id' => '62',
                'code' => 'HTG',
                'name' => 'Haitian Gourde',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            62 => 
            array (
                'id' => '63',
                'code' => 'HUF',
                'name' => 'Hungarian Forint',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            63 => 
            array (
                'id' => '64',
                'code' => 'IDR',
                'name' => 'Indonesian Rupiah',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            64 => 
            array (
                'id' => '65',
                'code' => 'ILS',
                'name' => 'Israeli New Sheqel',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            65 => 
            array (
                'id' => '66',
                'code' => 'IMP',
                'name' => 'Manx pound',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            66 => 
            array (
                'id' => '67',
                'code' => 'INR',
                'name' => 'Indian Rupee',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            67 => 
            array (
                'id' => '68',
                'code' => 'IQD',
                'name' => 'Iraqi Dinar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            68 => 
            array (
                'id' => '69',
                'code' => 'IRR',
                'name' => 'Iranian Rial',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            69 => 
            array (
                'id' => '70',
                'code' => 'ISK',
                'name' => 'Icelandic Króna',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            70 => 
            array (
                'id' => '71',
                'code' => 'JEP',
                'name' => 'Jersey Pound',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            71 => 
            array (
                'id' => '72',
                'code' => 'JMD',
                'name' => 'Jamaican Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            72 => 
            array (
                'id' => '73',
                'code' => 'JOD',
                'name' => 'Jordanian Dinar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            73 => 
            array (
                'id' => '74',
                'code' => 'JPY',
                'name' => 'Japanese Yen',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            74 => 
            array (
                'id' => '75',
                'code' => 'KES',
                'name' => 'Kenyan Shilling',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            75 => 
            array (
                'id' => '76',
                'code' => 'KGS',
                'name' => 'Kyrgystani Som',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            76 => 
            array (
                'id' => '77',
                'code' => 'KHR',
                'name' => 'Cambodian Riel',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            77 => 
            array (
                'id' => '78',
                'code' => 'KMF',
                'name' => 'Comorian Franc',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            78 => 
            array (
                'id' => '79',
                'code' => 'KPW',
                'name' => 'North Korean Won',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            79 => 
            array (
                'id' => '80',
                'code' => 'KRW',
                'name' => 'South Korean Won',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            80 => 
            array (
                'id' => '81',
                'code' => 'KWD',
                'name' => 'Kuwaiti Dinar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            81 => 
            array (
                'id' => '82',
                'code' => 'KYD',
                'name' => 'Cayman Islands Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            82 => 
            array (
                'id' => '83',
                'code' => 'KZT',
                'name' => 'Kazakhstani Tenge',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            83 => 
            array (
                'id' => '84',
                'code' => 'LAK',
                'name' => 'Laotian Kip',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            84 => 
            array (
                'id' => '85',
                'code' => 'LBP',
                'name' => 'Lebanese Pound',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            85 => 
            array (
                'id' => '86',
                'code' => 'LKR',
                'name' => 'Sri Lankan Rupee',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            86 => 
            array (
                'id' => '87',
                'code' => 'LRD',
                'name' => 'Liberian Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            87 => 
            array (
                'id' => '88',
                'code' => 'LSL',
                'name' => 'Lesotho Loti',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            88 => 
            array (
                'id' => '89',
                'code' => 'LTL',
                'name' => 'Lithuanian Litas',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            89 => 
            array (
                'id' => '90',
                'code' => 'LVL',
                'name' => 'Latvian Lats',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            90 => 
            array (
                'id' => '91',
                'code' => 'LYD',
                'name' => 'Libyan Dinar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            91 => 
            array (
                'id' => '92',
                'code' => 'MAD',
                'name' => 'Moroccan Dirham',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            92 => 
            array (
                'id' => '93',
                'code' => 'MDL',
                'name' => 'Moldovan Leu',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            93 => 
            array (
                'id' => '94',
                'code' => 'MGA',
                'name' => 'Malagasy Ariary',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            94 => 
            array (
                'id' => '95',
                'code' => 'MKD',
                'name' => 'Macedonian Denar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            95 => 
            array (
                'id' => '96',
                'code' => 'MMK',
                'name' => 'Myanma Kyat',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            96 => 
            array (
                'id' => '97',
                'code' => 'MNT',
                'name' => 'Mongolian Tugrik',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            97 => 
            array (
                'id' => '98',
                'code' => 'MOP',
                'name' => 'Macanese Pataca',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            98 => 
            array (
                'id' => '99',
                'code' => 'MRO',
                'name' => 'Mauritanian Ouguiya',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            99 => 
            array (
                'id' => '100',
                'code' => 'MTL',
                'name' => 'Maltese Lira',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            100 => 
            array (
                'id' => '101',
                'code' => 'MUR',
                'name' => 'Mauritian Rupee',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            101 => 
            array (
                'id' => '102',
                'code' => 'MVR',
                'name' => 'Maldivian Rufiyaa',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            102 => 
            array (
                'id' => '103',
                'code' => 'MWK',
                'name' => 'Malawian Kwacha',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            103 => 
            array (
                'id' => '104',
                'code' => 'MXN',
                'name' => 'Mexican Peso',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            104 => 
            array (
                'id' => '105',
                'code' => 'MYR',
                'name' => 'Malaysian Ringgit',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            105 => 
            array (
                'id' => '106',
                'code' => 'MZN',
                'name' => 'Mozambican Metical',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            106 => 
            array (
                'id' => '107',
                'code' => 'NAD',
                'name' => 'Namibian Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            107 => 
            array (
                'id' => '108',
                'code' => 'NGN',
                'name' => 'Nigerian Naira',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            108 => 
            array (
                'id' => '109',
                'code' => 'NIO',
                'name' => 'Nicaraguan Córdoba',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            109 => 
            array (
                'id' => '110',
                'code' => 'NOK',
                'name' => 'Norwegian Krone',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            110 => 
            array (
                'id' => '111',
                'code' => 'NPR',
                'name' => 'Nepalese Rupee',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            111 => 
            array (
                'id' => '112',
                'code' => 'NZD',
                'name' => 'New Zealand Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            112 => 
            array (
                'id' => '113',
                'code' => 'OMR',
                'name' => 'Omani Rial',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            113 => 
            array (
                'id' => '114',
                'code' => 'PAB',
                'name' => 'Panamanian Balboa',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            114 => 
            array (
                'id' => '115',
                'code' => 'PEN',
                'name' => 'Peruvian Nuevo Sol',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            115 => 
            array (
                'id' => '116',
                'code' => 'PGK',
                'name' => 'Papua New Guinean Kina',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            116 => 
            array (
                'id' => '117',
                'code' => 'PHP',
                'name' => 'Philippine Peso',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            117 => 
            array (
                'id' => '118',
                'code' => 'PKR',
                'name' => 'Pakistani Rupee',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            118 => 
            array (
                'id' => '119',
                'code' => 'PLN',
                'name' => 'Polish Zloty',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            119 => 
            array (
                'id' => '120',
                'code' => 'PYG',
                'name' => 'Paraguayan Guarani',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            120 => 
            array (
                'id' => '121',
                'code' => 'QAR',
                'name' => 'Qatari Rial',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            121 => 
            array (
                'id' => '122',
                'code' => 'RON',
                'name' => 'Romanian Leu',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            122 => 
            array (
                'id' => '123',
                'code' => 'RSD',
                'name' => 'Serbian Dinar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            123 => 
            array (
                'id' => '124',
                'code' => 'RUB',
                'name' => 'Russian Ruble',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            124 => 
            array (
                'id' => '125',
                'code' => 'RWF',
                'name' => 'Rwandan Franc',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            125 => 
            array (
                'id' => '126',
                'code' => 'SAR',
                'name' => 'Saudi Riyal',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            126 => 
            array (
                'id' => '127',
                'code' => 'SBD',
                'name' => 'Solomon Islands Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            127 => 
            array (
                'id' => '128',
                'code' => 'SCR',
                'name' => 'Seychellois Rupee',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            128 => 
            array (
                'id' => '129',
                'code' => 'SDG',
                'name' => 'Sudanese Pound',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            129 => 
            array (
                'id' => '130',
                'code' => 'SEK',
                'name' => 'Swedish Krona',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            130 => 
            array (
                'id' => '131',
                'code' => 'SGD',
                'name' => 'Singapore Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            131 => 
            array (
                'id' => '132',
                'code' => 'SHP',
                'name' => 'Saint Helena Pound',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            132 => 
            array (
                'id' => '133',
                'code' => 'SLL',
                'name' => 'Sierra Leonean Leone',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            133 => 
            array (
                'id' => '134',
                'code' => 'SOS',
                'name' => 'Somali Shilling',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            134 => 
            array (
                'id' => '135',
                'code' => 'SRD',
                'name' => 'Surinamese Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            135 => 
            array (
                'id' => '136',
                'code' => 'STD',
                'name' => 'São Tomé and Príncipe Dobra',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            136 => 
            array (
                'id' => '137',
                'code' => 'SVC',
                'name' => 'Salvadoran Colón',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            137 => 
            array (
                'id' => '138',
                'code' => 'SYP',
                'name' => 'Syrian Pound',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            138 => 
            array (
                'id' => '139',
                'code' => 'SZL',
                'name' => 'Swazi Lilangeni',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            139 => 
            array (
                'id' => '140',
                'code' => 'THB',
                'name' => 'Thai Baht',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            140 => 
            array (
                'id' => '141',
                'code' => 'TJS',
                'name' => 'Tajikistani Somoni',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            141 => 
            array (
                'id' => '142',
                'code' => 'TMT',
                'name' => 'Turkmenistani Manat',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            142 => 
            array (
                'id' => '143',
                'code' => 'TND',
                'name' => 'Tunisian Dinar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            143 => 
            array (
                'id' => '144',
                'code' => 'TOP',
                'name' => 'Tongan Paʻanga',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            144 => 
            array (
                'id' => '145',
                'code' => 'TRY',
                'name' => 'Turkish Lira',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            145 => 
            array (
                'id' => '146',
                'code' => 'TTD',
                'name' => 'Trinidad and Tobago Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            146 => 
            array (
                'id' => '147',
                'code' => 'TWD',
                'name' => 'New Taiwan Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            147 => 
            array (
                'id' => '148',
                'code' => 'TZS',
                'name' => 'Tanzanian Shilling',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            148 => 
            array (
                'id' => '149',
                'code' => 'UAH',
                'name' => 'Ukrainian Hryvnia',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            149 => 
            array (
                'id' => '150',
                'code' => 'UGX',
                'name' => 'Ugandan Shilling',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            150 => 
            array (
                'id' => '151',
                'code' => 'USD',
                'name' => 'United States Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            151 => 
            array (
                'id' => '152',
                'code' => 'UYU',
                'name' => 'Uruguayan Peso',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            152 => 
            array (
                'id' => '153',
                'code' => 'UZS',
                'name' => 'Uzbekistan Som',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            153 => 
            array (
                'id' => '154',
                'code' => 'VEF',
                'name' => 'Venezuelan Bolívar Fuerte',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            154 => 
            array (
                'id' => '155',
                'code' => 'VND',
                'name' => 'Vietnamese Dong',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            155 => 
            array (
                'id' => '156',
                'code' => 'VUV',
                'name' => 'Vanuatu Vatu',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            156 => 
            array (
                'id' => '157',
                'code' => 'WST',
                'name' => 'Samoan Tala',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            157 => 
            array (
                'id' => '158',
                'code' => 'XAF',
                'name' => 'CFA Franc BEAC',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            158 => 
            array (
                'id' => '159',
                'code' => 'XAG',
            'name' => 'Silver (troy ounce)',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            159 => 
            array (
                'id' => '160',
                'code' => 'XAU',
            'name' => 'Gold (troy ounce)',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            160 => 
            array (
                'id' => '161',
                'code' => 'XCD',
                'name' => 'East Caribbean Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            161 => 
            array (
                'id' => '162',
                'code' => 'XDR',
                'name' => 'Special Drawing Rights',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            162 => 
            array (
                'id' => '163',
                'code' => 'XOF',
                'name' => 'CFA Franc BCEAO',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            163 => 
            array (
                'id' => '164',
                'code' => 'XPD',
                'name' => 'Palladium Ounce',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            164 => 
            array (
                'id' => '165',
                'code' => 'XPF',
                'name' => 'CFP Franc',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            165 => 
            array (
                'id' => '166',
                'code' => 'XPT',
                'name' => 'Platinum Ounce',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            166 => 
            array (
                'id' => '167',
                'code' => 'YER',
                'name' => 'Yemeni Rial',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            167 => 
            array (
                'id' => '168',
                'code' => 'ZAR',
                'name' => 'South African Rand',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            168 => 
            array (
                'id' => '169',
                'code' => 'ZMK',
            'name' => 'Zambian Kwacha (pre-2013)',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            169 => 
            array (
                'id' => '170',
                'code' => 'ZMW',
                'name' => 'Zambian Kwacha',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            170 => 
            array (
                'id' => '171',
                'code' => 'ZWL',
                'name' => 'Zimbabwean Dollar',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
        ));
        
        
    }
}