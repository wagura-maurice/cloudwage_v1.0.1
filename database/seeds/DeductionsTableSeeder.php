<?php

use Illuminate\Database\Seeder;

class DeductionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('deductions')->delete();
        
        \DB::table('deductions')->insert(array (
            0 => 
            array (
                'id' => '1',
                'name' => 'PAYE',
                'due_date' => '9',
                'type' => 'slab',
                'threshold' => '11135.0',
                'rate' => NULL,
                'has_relief' => '1',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            1 => 
            array (
                'id' => '2',
                'name' => 'NHIF',
                'due_date' => '9',
                'type' => 'slab',
                'threshold' => '0.0',
                'rate' => NULL,
                'has_relief' => '0',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
            2 => 
            array (
                'id' => '3',
                'name' => 'NSSF',
                'due_date' => '15',
                'type' => 'slab',
                'threshold' => '0.0',
                'rate' => NULL,
                'has_relief' => '0',
                'created_at' => '2024-12-05 19:20:24',
                'updated_at' => '2024-12-05 19:20:24',
            ),
        ));
        
        
    }
}