<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CurrencyTableSeeder::class);
        $this->call(CompanyProfilesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AllowancesTableSeeder::class);
        $this->call(DeductionsTableSeeder::class);
        $this->call(DeductionSlabsTableSeeder::class);
        $this->call(ReliefsTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(PoliciesTableSeeder::class);
        $this->call(PaymentMethodsTableSeeder::class);
        $this->call(PaymentStructuresTableSeeder::class);
        $this->call(ShiftsTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(EmployeeTypesTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(UdfsTableSeeder::class);
        $this->call(WorkPlansTableSeeder::class);
        $this->call(ReportTemplatesTableSeeder::class);
        $this->call(HolidaysTableSeeder::class);
        $this->call(PayGradesTableSeeder::class);
        $this->call(BranchesTableSeeder::class);
        $this->call(DepartmentsTableSeeder::class);
    }
}
