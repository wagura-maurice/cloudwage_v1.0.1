<?php

use App\LightOfGuidance;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLightOfGuidancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('light_of_guidances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('_uid')->unique();
            $table->unsignedBigInteger('user_id')->nullable(); // Define the foreign key column
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null'); // Add the foreign key constraint
            $table->string('_class');
            $table->text('message');
            $table->text('trace')->nullable();
            $table->string('exception_type')->nullable();
            $table->integer('exception_code')->nullable();
            $table->string('request_info')->nullable();
            $table->string('job_class')->nullable();
            $table->integer('job_id')->nullable();
            $table->string('queue_name')->nullable();
            $table->string('queue_connection')->nullable();
            $table->string('model_class')->nullable();
            $table->integer('model_id')->nullable();
            $table->text('payload')->nullable();
            $table->string('environment')->nullable();
            $table->integer('_status')->default(LightOfGuidance::PENDING);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('light_of_guidances');
    }
}
