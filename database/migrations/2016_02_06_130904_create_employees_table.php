<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id'); // Auto-incrementing ID
            $table->string('payroll_number')->nullable()->unique(); // Unique payroll number
            $table->string('avatar')->nullable(); // Avatar field, made nullable
            $table->string('identification_number'); // Identification number
            $table->enum('identification_type', ['National ID', 'Passport']); // Enum for ID type
            $table->string('first_name'); // First name
            $table->string('middle_name')->nullable(); // Middle name, made nullable
            $table->string('last_name')->nullable(); // Last name, made nullable
            $table->string('email')->unique(); // Unique email address
            $table->string('mobile_phone', 13)->unique(); // Unique mobile phone number with length limit
            $table->integer('payment_structure_id')->unsigned(); // Foreign key for payment structure
            $table->boolean('has_custom_tax_rate')->default(false); // Custom tax rate flag
            $table->integer('custom_tax_rate')->default(0); // Custom tax rate amount
            $table->softDeletes(); // Soft delete functionality (termination date)
            $table->timestamps(); // Created at and updated at timestamps
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }
}
