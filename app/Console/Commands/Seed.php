<?php
// app/Console/Commands/Seed.php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Seed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed the applications core database tables';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            $this->info('Seeding the application...');

            // Ensure the command uses /usr/bin/php7.1 for the artisan call
            $phpPath = '/usr/bin/php7.1'; // Define the path to PHP 7.1

            // Use the PHP path in the artisan call
            $command = $phpPath . ' artisan iseed payment_methods,payment_structures,branches,shifts,currencies,reliefs,allowances,employee_types,departments,pay_grades,modules,settings,deductions,deduction_slabs,udfs,work_plans,holidays,policies,report_templates --force';

            // Execute the command
            exec($command, $output, $status);

            // Check if the command ran successfully
            if ($status === 0) {
                $this->info('Application seeding completed successfully!');
            } else {
                $this->error('Seeding failed with error status ' . $status);
                $this->error('Output: ' . implode("\n", $output));
            }
        } catch (\Throwable $th) {
            // Handle the error
            eThrowable(get_class($this), $th->getMessage(), $th->getTraceAsString());

            $this->error('An error occurred during seeding: ' . $th->getMessage());
            $this->error('Stack Trace: ' . $th->getTraceAsString());
        }
    }
}
