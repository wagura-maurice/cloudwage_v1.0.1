<?php
// app/Console/Commands/FreshMigrate.php
namespace App\Console\Commands;

use Illuminate\Console\Command;

class FreshMigrate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:fresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drop all tables and re-run migrations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('migrate:reset');
        $this->call('migrate');
        $this->call('db:seed');
        $this->info('Database has been reset, migrations re-run and seeding!');
    }
}
