<?php
// app/Console/Commands/Debug.php
namespace App\Console\Commands;

use Payroll\Models\Employee;
use Illuminate\Console\Command;

class Debug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:debug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Debugging the application';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            
            $employees = Employee::with('contract')->get();

            /* foreach ($employees as $key => $employee) {
                dd($employee->contract->start_date, $employee->contract->end_date);

                genven that we know a employess start sart of contact

                genere a list down of all the months like below


                e.g if my start dat is 24th jun 2024

                return a array os string like 

                    jun_2024
            } */

            // DaysWorked::create();

            $this->info('Application debugging completed successfully!');
        } catch (\Throwable $th) {
            // throw $th;
            eThrowable(get_class($this), $th->getMessage(), $th->getTraceAsString());
            
            $this->error('An error occurred during debugging: ' . $th->getMessage());
            $this->error('Stack Trace: ' . $th->getTraceAsString());
        }
    }
}
