<?php
// app/Console/Commands/Optimize.php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Optimize extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:optimize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Optimizing the application';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            $this->info('Putting the application in maintenance mode...');
            Artisan::call('down');

            $this->info('Clearing cache...');
            Artisan::call('cache:clear');

            $this->info('Clearing route cache...');
            Artisan::call('route:clear');

            $this->info('Caching routes...');
            Artisan::call('route:cache');

            $this->info('Clearing configuration cache...');
            Artisan::call('config:clear');

            $this->info('Caching configuration...');
            Artisan::call('config:cache');

            $this->info('Clearing compiled views...');
            Artisan::call('view:clear');

            $this->info('Running optimize...');
            Artisan::call('optimize');

            $this->info('Running composer dump-autoload...');
            shell_exec('/usr/bin/php7.1 /usr/local/bin/composer dump-autoload');

            $this->info('Bringing the application out of maintenance mode...');
            Artisan::call('up');

            $this->info('Application optimization completed successfully!');
        } catch (\Throwable $th) {
            // throw $th;
            eThrowable(get_class($this), $th->getMessage(), $th->getTraceAsString());
            
            $this->error('An error occurred during optimization: ' . $th->getMessage());
            $this->error('Stack Trace: ' . $th->getTraceAsString());
        }
    }
}
