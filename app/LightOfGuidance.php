<?php

namespace App;

use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LightOfGuidance extends Model
{
    use SoftDeletes;

    // status
    const PENDING = 0;
    const OPEN = 1;
    const CLOSED = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_uid',
        'user_id',
        '_class',
        'message',
        'trace',
        'exception_type',
        'exception_code',
        'request_info',
        'job_class',
        'job_id',
        'queue_name',
        'queue_connection',
        'model_class',
        'model_id',
        'payload',
        'environment',
        '_status'
    ];

    public static function createRules(): array
    {
        return [
            '_uid' => ['required', 'string', Rule::unique('light_of_guidances', '_uid')],
            'user_id' => 'nullable|exists:users,id',
            '_class' => 'required|string',
            'message' => 'required|string',
            'trace' => 'nullable|string',
            'exception_type' => 'nullable|string',
            'exception_code' => 'nullable|integer',
            'request_info' => 'nullable|json',
            'job_class' => 'nullable|string',
            'job_id' => 'nullable|integer',
            'queue_name' => 'nullable|string',
            'queue_connection' => 'nullable|string',
            'model_class' => 'nullable|string',
            'model_id' => 'nullable|integer',
            'payload' => 'nullable|text',
            'environment' => 'nullable|string',
            '_status' => 'nullable|integer'
        ];
    }
    
    public static function updateRules(int $id): array
    {
        return [
            '_uid' => ['nullable', 'string', Rule::unique('light_of_guidances', '_uid')->ignore($id)],
            'user_id' => 'nullable|exists:users,id',
            '_class' => 'nullable|string',
            'message' => 'nullable|string',
            'trace' => 'nullable|string',
            'exception_type' => 'nullable|string',
            'exception_code' => 'nullable|integer',
            'request_info' => 'nullable|json',
            'job_class' => 'nullable|string',
            'job_id' => 'nullable|integer',
            'queue_name' => 'nullable|string',
            'queue_connection' => 'nullable|string',
            'model_class' => 'nullable|string',
            'model_id' => 'nullable|integer',
            'payload' => 'nullable|text',
            'environment' => 'nullable|string',
            '_status' => 'nullable|integer'
        ];
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
