<?php

use Carbon\Carbon;
use Payroll\Models\Employee;
use Payroll\Models\Holiday;



if (! function_exists('atProcessingStatus')) {
    function atProcessingStatus(int $value): string
    {
        return collect([
            'PROCESS'   => 0,
            'PROCESSED' => 1,
            'SUCCESS'   => 2,
            'DELIVERED' => 2
        ])->pull(strtoupper($value)) ?? 1;
    }
}

if (!function_exists('isVerifiedMemberNumber')) {
    function isVerifiedMemberNumber(string $id): bool
    {
        // Check if the string starts with "CW-", case-insensitively
        return preg_match('/^CW-/i', $id) === 1;
    }
}

if (!function_exists('generateEmployeeNumber')) {
    function generateEmployeeNumber(): string
    {
        $timestamp = Carbon::parse(REQUEST_TIMESTAMP);
        
        // Use max ID directly for sequential incrementation
        $sequence = Employee::max('id') + 1;
        
        do {
            // Format member number
            $number = sprintf(
                "CW-%d-%s-%s",
                $sequence,               // Sequential number
                $timestamp->format('Y'), // Year
                $timestamp->format('m')  // Month
            );

            // Only proceed to the next sequence if this _uid already exists
            if (Employee::where('_uid', $number)->exists()) {
                $sequence++;
            } else {
                break; // Exit loop if unique number is found
            }
        } while (true);

        return $number;
    }
}

if (!function_exists('generatePayrollNumber')) {
    function generatePayrollNumber(string $clientCode): string
    {
        $timestamp = Carbon::parse(REQUEST_TIMESTAMP);

        // Start with the next available ID as the sequence number
        $sequence = Employee::max('id') + 1;

        do {
            // Format payroll number
            $number = sprintf(
                "PN-%d-%s-%s",
                $sequence,                 // Sequential number
                $timestamp->format('Ymd'), // Date element in YYYYMMDD format
                $clientCode                // Client or account code
            );

            // Check for uniqueness and increment sequence only if duplicate exists
            if (Employee::where('_uid', $number)->exists()) {
                $sequence++;
            } else {
                break; // Unique number found, exit loop
            }
        } while (true);

        return $number;
    }
}
