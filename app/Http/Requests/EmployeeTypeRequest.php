<?php
// app/Http/Requests/EmployeeTypeRequest.php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PUT') {
            $employeeTypeId = $this->route('employee_type'); // Assuming the route parameter is `employee_type`
            return [
                'name' => 'required|min:3|unique:employee_types,name,' . $employeeTypeId,
                'payment_structure_id' => 'required|numeric',
                'description' => 'required|min:20'
            ];
        }

        return [
            'name' => 'required|unique:employee_types,name',
            'payment_structure_id' => 'required|numeric',
            'description' => 'required|min:20'
        ];
    }
}
