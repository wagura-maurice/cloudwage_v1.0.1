<?php
// app/Http/Controllers/Auth/LoginController.php
namespace App\Http\Controllers\Auth;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function email()
    {
        return 'email';
    }

    protected function authenticated(Request $request, User $user)
    {
        try {
            $user->update([
                'last_login' => Carbon::now(),
            ]);
        } catch (\Exception $e) {
            \Log::error('Failed to update last login: ' . $e->getMessage());
        }

        return redirect()->intended($this->redirectTo);
    }

    protected function validateLogin(Request $request)
    {
        Validator::make($request->all(), [
            $this->email() => 'required|string|exists:users,email',
            'password' => 'required|string',
        ])->validate();
    }
    
    protected function loggedOut(Request $request)
    {
        return redirect('/login');
    }
}

