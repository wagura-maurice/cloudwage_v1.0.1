<?php

namespace App\Providers;

use Carbon\Carbon;
use Payroll\Models\Payroll;
use Payroll\Models\Allowance;
use Payroll\Models\Deduction;
use Payroll\Models\CompanyProfile;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Payroll\Repositories\PolicyRepository;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // App Environment.
        /* if (env('APP_ENV') !== 'local') {
            URL::forceScheme('https');
        } */

        // Define a global timestamp for the request cycle
        if (!defined('REQUEST_TIMESTAMP')) {
            define('REQUEST_TIMESTAMP', Carbon::now());
        }
        
        Schema::defaultStringLength(191);

        view()->composer(['layout'], function ($view) {
            $company = CompanyProfile::first();
            $daysPolicy = PolicyRepository::get(Payroll::MODULE_ID, Payroll::ENABLE_DAYS_ATTENDANCE);

            $view->withCompany($company)
                ->withDaysEnabled($daysPolicy);
        });

        Relation::morphMap([
            'Allowance' => Allowance::class,
            'Deduction' => Deduction::class,
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // 
    }
}
